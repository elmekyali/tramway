package com.sqli.test.tramway;

public class YoungMan extends Passenger {

    @Override
    public int getTime() {
        return canGetOn ? 1 : 0;
    }

    @Override
    public void setCanGetOn(boolean isBlocked) {
        this.canGetOn = !isBlocked;
    }
}
