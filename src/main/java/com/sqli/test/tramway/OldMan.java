package com.sqli.test.tramway;

public class OldMan extends Passenger {

    @Override
    public int getTime() {
        return canGetOn ? 2 : 0;
    }

    @Override
    public void setCanGetOn(boolean isBlocked) {
        this.canGetOn = !isBlocked;
    }
}
