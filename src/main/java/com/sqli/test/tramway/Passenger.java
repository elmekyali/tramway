package com.sqli.test.tramway;

public abstract class Passenger {
    boolean canGetOn;

    public abstract int getTime();

    public abstract void setCanGetOn(boolean isBlocked);
}
