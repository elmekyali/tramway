package com.sqli.test.tramway;

import java.util.LinkedList;
import java.util.List;

public class TramLine {

    private static final String DEFAULT_STATION = "Terminus";
    private List<Station> stationList;
    private Station currentStation;

    public TramLine() {
        stationList = new LinkedList<>();
        currentStation = new Station(DEFAULT_STATION);
        stationList.add(currentStation);
    }

    public void addStation(String stationName, String... passengers) {
        this.stationList.add(new Station(stationName, passengers));
    }

    public String getCurrentStation() {
        return currentStation.getName();
    }

    public int pickAndRun() {
        int timeToRun = (currentStation.isBlocked() ? 0 : 2);
        boolean canAllowToMoving = currentStation.isBlocked();
        int timeToPick = currentStation.getTimeToPick();
        currentStation = canAllowToMoving ? currentStation : nextStation();
        return timeToPick + timeToRun;
    }

    private Station nextStation() {
        return stationList.get(stationList.indexOf(currentStation) + 1);
    }
}
