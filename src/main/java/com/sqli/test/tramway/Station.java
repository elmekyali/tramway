package com.sqli.test.tramway;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Station {
    private String name;
    private List<Passenger> passengers;
    private boolean isBlocked;

    public Station(String stationName, String... passengers) {

        this.name = stationName;

        this.passengers = Arrays.stream(passengers)
                .flatMap(passenger -> passengerParser(passenger).stream())
                .collect(Collectors.toList());
    }

    private List<Passenger> passengerParser(String passengerObject) {

        String[] token = passengerObject.split(" ");
        int numberOfPassengers = 1;

        if (token.length > 1){
            numberOfPassengers = Integer.parseInt(token[0]);
        }

        Passenger passenger = passengerObject.contains("youngMan") ? new YoungMan() :
                (passengerObject.contains("oldMan") ? new OldMan() : new Disturber());

        isBlocked = passengerObject.contains("disturber") || isBlocked;

        return Collections.nCopies(numberOfPassengers, passenger);
    }

    public String getName() {
        return name;
    }

    public int getTimeToPick() {
        this.passengers.forEach(passenger -> passenger.setCanGetOn(isBlocked));
        this.isBlocked = false;
        return passengers.stream().mapToInt(Passenger::getTime).sum();
    }

    public boolean isBlocked() {
        return isBlocked;
    }
}
