package com.sqli.test.tramway;

public class Tram {

    private final TramLine tramLine;
    private int tripDuration;

    public Tram(TramLine tramLine) {
        this.tramLine = tramLine;
        this.tripDuration = 0;
    }

    public String getCurrentStation() {
        return tramLine.getCurrentStation();
    }

    public int pickAndRun() {
        return tripDuration += tramLine.pickAndRun();
    }
}
