package com.sqli.test.tramway;

public class Disturber extends Passenger {

    @Override
    public int getTime() {
        return canGetOn ? 3 : 0;
    }

    @Override
    public void setCanGetOn(boolean isBlocked) {
        this.canGetOn = isBlocked;
    }
}
